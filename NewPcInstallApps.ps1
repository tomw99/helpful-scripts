# Set-ExecutionPolicy remote

function checkLastCommand {
    if (!$?) {
        Write-Output 'Error running last command. existing.'
        Set-Content -Path 'step' -Value $args[0]
        exit 1
    }
}
function install {
    $package = $args[0]
    $checkOutput = (choco list $package -le)
    if ($checkOutput -eq '0 packages installed.') {
        Write-Output 'Installing: $package'
        choco install -y --no-progress $package -Confirm
        checkLastCommand $package
    }
    else {
        Write-Output '$package already installed'
    }
}

Set-Variable -Name apps -Value $(
    'chocolatey-core.extension',
    'nuget.commandline',
    'intel-dsa',
    'openssh',
    'nodejs.install',
    'sysinternals'
    'git.install',
    'googlechrome',
    'firefox',
    '7zip.install',
    'notepadplusplus.install',
    'ccleaner',
    'paint.net',
    'iobit-uninstaller',
    'smplayer',
    'veracrypt',
    'nmap',
    'media-preview'
    'vscode',
    'pia',
    'manycam'
)

Set-Variable -Name startingStep -Value $apps[0]
$loc = split-path -parent $MyInvocation.MyCommand.Definition
$loc = '$loc\step'
if (Test-path -Path $loc) {
    Set-Variable -Name startingStep -Value (Get-Content $loc)
    Write-Output 'Detected incomplete installations starting from $startingStep'
    Remove-Item -Path $loc
}


function chocoInstall {
    [CmdletBinding(
        SupportsShouldProcess = $true,
        ConfirmImpact = 'High')]
    param()
    Write-Output 'Installing applications'
    $isStartingStep = 1
    foreach ($app in $apps) {
        if ($isStartingStep) {
            if ($app -eq $startingStep) {
                $isStartingStep = 0
            }
            else {
                Write-Output 'Skipping $app. It has already been installed.'
                continue
            }
        }
        if ($PSCmdlet.ShouldProcess($app, 'choco install')) {
            install $app
        }
    }
    Write-Host 'Application installs completed.'
}
#chocoInstall



$RegKeySets = @{
  'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' = @{
      'Hidden' = 1;
      'HideFileExt' = 0;
      'ShowSuperHidden' = 0;
      'hidedriveswithnomedia' = 0;
      'hidemergeconflicts' = 1;
      'autocheckselect' = 1;
      'SeparateProcess' = 1;
      'ShowCortanaButton' = 0;
  };
  'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer' = @{
    'HubMode' = 1; #Remove quick access.
  }
 };




function setExplorerOptions {
    [CmdletBinding(
        SupportsShouldProcess = $true,
        ConfirmImpact = 'High')]
    param()
    Write-Output 'Settings windows explorer settings.'
    foreach($regKeySet in $RegKeySets.Keys) {
    $regKeys = $RegKeySets[$regKeySet];
    foreach($property in $regKeys.Keys) {
       if ($PSCmdlet.ShouldProcess($property, 'Set Propery')) {
          Set-ItemProperty $regKeySet $property $regKeys[$property]
       } 
    }
    }
    Stop-Process -processname explorer
    Write-Output 'Windows explorer settings completed.'
}
setExplorerOptions

Write-Output 'Completed.'
